import requests
import os
import sys
import mlflow
def send(url,runid):
    """
    The function to trigger the GitLab-MLflow plugin
    url: The API Endpoint of the GitLab-MLflow plugin
    runid: The runid of the MLflow run to be passed
    """
    mrid=os.getenv("CI_MERGE_REQUEST_IID")
    projectid=os.getenv("CI_MERGE_REQUEST_PROJECT_ID")
    if mrid is None:
        dst=mlflow.artifacts.download_artifacts(run_id=runid,dst_path="./artifacts")
        if dst is None:
            sys.exit(1)
        else:
            sys.exit(0)
    data={"runid":runid,"mrid":mrid,"projectid":projectid}
    requests.post(url,data=data)



