import setuptools

setuptools.setup(
    name="pgmlflow",
    version="1.2.9",
    author="zt",
    author_email="contact-project+academy-fyp-pgmlflow-95482-issue-@mg.jihulab.com",
    description="A package to connect the MLflow and GitLab",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
